package ru.kadabeld.yavm.presentation.view.login;

import com.arellomobile.mvp.MvpView;

public interface LoginView extends MvpView {
    void onVkAppFound();

    void showLoginDialog();

    void show2StepAuth(boolean isApp);

    void onLoginFailed();

    void on2StepAuthFailed(boolean isApp);

    void onLogin();

    void sdkLogin(String... scope);

    void dismissDialog();
}
