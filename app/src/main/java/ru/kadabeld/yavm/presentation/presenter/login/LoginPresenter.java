package ru.kadabeld.yavm.presentation.presenter.login;

import android.content.Context;
import android.content.pm.PackageManager;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;

import ru.kadabeld.yavm.presentation.view.login.LoginView;
import ru.kadabeld.yavm.presentation.view.login.LoginView$$State;

@InjectViewState
public class LoginPresenter extends MvpPresenter<LoginView> {

    private static final String[] SCOPE = new String[]{
            VKScope.FRIENDS,
            VKScope.PHOTOS,
            VKScope.MESSAGES,
            VKScope.DOCS,
            VKScope.GROUPS,
    };

    @Override
    public void attachView(LoginView view) {
        super.attachView(view);
        if (VKSdk.isLoggedIn()) {
            view.onLogin();
        } else {
            try {
                ((Context) view).getPackageManager().getApplicationInfo("com.vkontakte.android", 0);
                // приложение вк имеется, сожем логиниться через него
                view.onVkAppFound();
            } catch (PackageManager.NameNotFoundException ignored) {
                // приложения вк нет, логинимся через логин-пароль
                view.showLoginDialog();
            }
        }
    }

    public void loginApp() {
        getViewState().sdkLogin(SCOPE);
    }

    public void loginViaCredits() {
        getViewState().showLoginDialog();
    }
}
