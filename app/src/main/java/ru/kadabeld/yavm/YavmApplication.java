package ru.kadabeld.yavm;

import android.app.Application;

import com.vk.sdk.VKSdk;

/**
 * Created by
 * vladislav
 * 12:15
 * 22.08.17
 */

public class YavmApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        VKSdk.initialize(this);
    }
}
