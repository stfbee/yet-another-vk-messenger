package ru.kadabeld.yavm.ui.activity.login;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.arellomobile.mvp.MvpActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vk.sdk.VKSdk;

import ru.kadabeld.yavm.R;
import ru.kadabeld.yavm.presentation.presenter.login.LoginPresenter;
import ru.kadabeld.yavm.presentation.view.login.LoginView;

public class LoginActivity extends MvpActivity implements LoginView {
    public static final String TAG = "LoginActivity";
    @InjectPresenter
    LoginPresenter mLoginPresenter;

    AlertDialog alertDialog;

    public static Intent getIntent(final Context context) {
        Intent intent = new Intent(context, LoginActivity.class);

        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getMvpDelegate().onAttach();
    }


    @Override
    public void onVkAppFound() {
        alertDialog = new AlertDialog.Builder(this, R.style.AppThemeDark)
                .setTitle("Авторизация")
                .setMessage("Добро пожаловать в еще один мессенджер для ВКонтакте.\n" +
                        "На твоем телефоне найдено приложение, которое можно использовать для входа, продолжить?")
                .setPositiveButton("Далее", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mLoginPresenter.loginApp();
                    }
                })
                .setNegativeButton("Другой аккаунт", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mLoginPresenter.loginViaCredits();
                    }
                })
                .show();
    }

    @Override
    public void showLoginDialog() {
        Toast.makeText(LoginActivity.this, "тут потом будет диалог ввода данных", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void show2StepAuth(boolean isApp) {

    }

    @Override
    public void onLoginFailed() {

    }

    @Override
    public void on2StepAuthFailed(boolean isApp) {

    }

    @Override
    public void onLogin() {
        Toast.makeText(LoginActivity.this, "login success", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void sdkLogin(String... scope) {
        VKSdk.login(this, scope);
    }

    @Override
    public void dismissDialog() {
        alertDialog.dismiss();
    }
}
