### Ещё один мессенджер для ВКонтакте ###
Open-source проект "для себя". Есть желание что-то изменить - присылайте пуллреквесты.

### Исползуемые технологии и библиотеки ###
* Android API ≥ 19 (KitKat)
* Git Flow
* RxJava
* Retrofit
* ButterKnife
* Picasso
* Fabric

Для запуска необходимо указать значения KEYSTORE_FILE, KEYSTORE_PASSWORD и KEY_PASSWORD в файле gradle.properties

Связаться со мной: [ВК](https://vk.com/im?sel=8043960)

Чуть позже будут добавлены арты-дизайны, sketch и psd файлы так же будут добавляться